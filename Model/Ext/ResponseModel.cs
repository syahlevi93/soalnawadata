﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Ext
{
  public  class ResponseModel
    {
        public string VowelCharacters { get; set; }
        public string ConsonantCharacters { get; set; }
    }
}
