﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Ext
{
   public class InputRequest
    {
        public int InputTheNumberofFamilies { get; set; }
        public string InputTheNumberofMembersInTheFamily { get; set; }
    }
}
