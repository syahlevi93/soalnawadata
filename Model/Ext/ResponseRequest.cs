﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Ext
{
    public class ResponseRequest
    {
        public int MinimumBusRequired { get; set; }
        public string Result { get; set; }

        public int CountFamily { get; set; }
    }
}
