﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceNawaData.ModelDTO
{
    public class ShortCharacterResponseDTO
    {
        public string VowelCharacters { get; set; }
        public string ConsonantCharacters { get; set; }
    }
}
