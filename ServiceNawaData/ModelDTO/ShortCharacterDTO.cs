﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceNawaData.ModelDTO
{
    public class ShortCharacterDTO
    {
        public string InputOneLineOfWords { get; set; }
    }
}
