﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Ext;
using Service.InterfaceExt;
using ServiceNawaData.ModelDTO;
using Model.Ext;

namespace ServiceNawaData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SortCharacterController : ControllerBase
    {
        private readonly IServiceShortCharacter service;

        public SortCharacterController(IServiceShortCharacter _service)
        {
            this.service = _service;
        }

        [HttpPost()]
        public ResponseModel GetData(RequestModel entity)
        {
            var data = service.GetData(entity);
            return data;
        }


        [HttpGet()]
        public int GetData2(RequestModel entity)
        {
            var data = service.getlenght(entity);
            return data;
        }

    }
}
