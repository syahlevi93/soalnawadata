﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Ext;
using Service.InterfaceExt;
using ServiceNawaData.ModelDTO;
using Model.Ext;
namespace ServiceNawaData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PSBBController : ControllerBase
    {
        private readonly IServiceShortCharacter service;

        public PSBBController(IServiceShortCharacter _service)
        {
            this.service = _service;
        }

        [HttpPost]
        public ResponseRequest GetPSBB(InputRequest entity)
        {
            var data = service.GetPSBB(entity);
            return data;
        }

    }
}
